<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('servicios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('serv', 50);
			$table->unsignedInteger('id_cliente');
			$table->string('observacion', 100);
			$table->timestamps();
			$table->foreign('id_cliente')->references('id')->on('users');
			$table->unique(['serv','id_cliente']);
		});
	}
protected $fillable = ['serv','id_cliente', 'observacion'];
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('servicios');
	}

}
