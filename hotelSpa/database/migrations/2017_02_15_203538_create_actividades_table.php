<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actividades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('actividad', 50);
			$table->unsignedInteger('id_cliente');
			$table->string('fecha', 8);
			$table->timestamps();
			$table->foreign('id_cliente')->references('id')->on('users');
			$table->unique(['actividad','id_cliente']);
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actividades');
	}

}
