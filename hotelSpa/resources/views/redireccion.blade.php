@extends('app')

@section('content')

<div id="redireccion">
    <div class="container">
        <div class="col-xs-12 center">
            <h1>Su registro ha sido guardado</h1>
            <h3>En breve le redireccionaremos a la página de inicio.</h3>
            <br>
            <img class="redirect" src="{{ asset('img/logos/reload.gif') }}">
        </div>
        </div>
    </div>
</div>
<script>redireccionar();</script>
@endsection