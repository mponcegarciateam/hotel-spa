@extends('app')

@section('content')
	<div id="home">
		<div class="container">
			<div class="row center">
				<br>
				<br>
				<div class="col-sm-12">
					<img class="img-responsive" src="{{ asset('/img/logos/hotelspaNegro.png') }}">
				</div>
			</div>
			<br>
			@if(Session::has('message'))
				<div class="col-md-6 col-md-offset-3">
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
						{{Session::get('message')}}
					</div>
				</div>
			@endif
			<br>
			<div class="row center">

				<div class="col-sm-4 box">
					<h2>Servicios</h2><br>
					<a href="{{ url('/servicios/comida') }}">
						<div class="box-item">
							<img src="{{ asset('/img/home/comida.png') }}">
							<h3>Petición de comida</h3>
						</div>
					</a>
					<a href="{{ url('/servicios/despertador') }}">
						<div class="box-item">
							<h3>Servicio de despertador</h3>
							<img src="{{ asset('/img/home/despertador.png') }}">
						</div>
					</a>
					<a href="{{ url('/servicios/limpieza') }}">
						<div class="box-item">
							<img src="{{ asset('/img/home/limpieza.png') }}">
							<h3>Servicio de limpieza</h3>
						</div>
					</a>
				</div>
				<div class="col-sm-4 box">
					<h2>Actividades</h2><br>
					<a href="{{ url('/actividades/gimnasio') }}">
						<div class="box-item">
							<img src="{{ asset('/img/home/gimnasio.png') }}">
							<h3>Gimnasio & fitness</h3>
						</div>
					</a>
					<a href="{{ url('/actividades/masajista') }}">
						<div class="box-item">
							<h3>Masajista & relax</h3>
							<img src="{{ asset('/img/home/masajista.png') }}">
						</div>
					</a>
					<a href="{{ url('/actividades/conferencias') }}">
						<div class="box-item">
							<img src="{{ asset('/img/home/conferencia.png') }}">
							<h3>Conferencias</h3>
						</div>
					</a>
					<a href="{{ url('/actividades/spa') }}">
						<div class="box-item">
							<h3>Spa & Jacuzzi</h3>
							<img src="{{ asset('/img/home/spa.png') }}">	
						</div>
					</a>
				</div>
				<div class="col-sm-4 box">
					<h2>Administración</h2><br>
					<a href="{{ url('/admin') }}">
						<div class="box-item">
							<div class="box-item">
								<img src="{{ asset('/img/home/administracion.png') }}">
								<h3>Panel de administración</h3>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<br><br>	
	</div>

@endsection
