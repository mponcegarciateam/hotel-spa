@extends('app')

@section('content')

  <div id="conferencias">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src= "{{asset('/img/conferencias/01.jpg')}}" alt="Conferencia01">
          <div class="carousel-caption">
            <h3>Conferencias IESE</h3>
            <p>Conferencias impartidas por los líderes de IESE</p>
          </div> 
        </div>

        <div class="item">
          <img src="{{asset('/img/conferencias/02.jpg')}}" alt="Conferencia02">
          <div class="carousel-caption">
            <h3>Edward Richtofen</h3>
            <p>Coaching deportivo número 1 nos motivará todos los miércoles.</p>
          </div> 
        </div>

        <div class="item">
          <img src="{{asset('/img/conferencias/03.jpg')}}" alt="Conferencia03">
          <div class="carousel-caption">
            <h3>Planificación de conferencias</h3>
            <p>Las conferencias se planifican minuciosamente con grandes profesionales. </p>
          </div> 
        </div>
      </div>
  <!-- Controles izq y dcha -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <br>
    <br>
  	<div class="container center conferencias"> 		
    	<div class="row">
          <div class="col-sm-4">
            <img src="{{asset('/img/conferencias/conferencias.png')}}">
            <br>
            <h2>Conferencia motivacional</h2>
            <br>
            <p>Nuestra conferencia motivacional aplica los valores más importantes del deporte, tratando temas como la motivación y superación.</p>
          </div>
          <div class="col-sm-4">
            <img src="{{asset('/img/conferencias/estudiar.png')}}">
            <br>
            <h2>Videoconferencias</h2>
            <br>
            <p>Las videoconferencia son una gran solución para maximizar tu tiempo, te presentamos la posibilidad de poder realizarlas desde ordenadores y dispositivos móviles.</p>
          </div>
          <div class="col-sm-4">
            <img src="{{asset('/img/conferencias/idioma.png')}}">
            <br>
            <h2>Idiomas</h2>
            <br>
            <p>Las videoconferencia estarán disponible en inglés, y las presenciales, dispondrá de auriculares que le traducirá a medida que transcurre la conferencia.</p>
          </div>
      </div>
    </div>
    <div class="container">
      <br>
        <br>
        @if(Session::has('message'))
            <div class="col-md-8 col-md-offset-2">
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
                {{Session::get('message')}}
              </div>
            </div>
          @endif
      <div class="row center">
        <div class="col-xs-6 col-xs-offset-3">
          <br>
         <form role="form" method="post">
            @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Error!</strong> No se ha podido reservar.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
            </ul>
          </div>
          @endif
                    <label>Reservar</label>
                    <div class="form-group">
                        <div class='center input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control" name="fecha" />
                            <span class="indigo blanco center input-group-addon">
                                <span class="center glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default" name="botonenviar">Reservar</button> 
            </form>
            <br>
            <br>
        </div>
      </div>
    </div>
    <br>
    <br>
  </div>

@endsection



