@extends('app')

@section('content')
<div id="masaje">
<div class="content">
  <div class="container">
    <div class="welcome">
    <br>
    <br>
      <div class="row center">
        <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col1">
          <h2>Actividad de masajes</h2>
          <p>En Hotel Spa ofrecemos una serie de actividades de masajes que te harán sentir como si estuvieras en el paraiso. Disfruta de todos nuestros servicios a tu alcance.</p>
        </div>
      </div>
      <div class="row center">
      <br>
        <br>
        <div class="col2">
          <div class="col-xs-6 col-sm-2 col-sm-offset-2 col1">
            <figure class="effect-bubba">
              <img class="img-responsive" src="{{ asset('img/Masaje/icon_1.png') }}" alt="" width="260" height="260" />
              <figcaption>
                <h4>Masaje clásico</h4>
              </figcaption>     
            </figure>
          </div>
          <div class="col-xs-6 col-sm-2 col1">
            <figure class="effect-bubba">
              <img class="img-responsive" src="{{ asset('img/Masaje/icon_3.png') }}" alt="" width="260" height="260"/>
              <figcaption>
                <h4>Terapia con piedras</h4>
              </figcaption>     
            </figure>
          </div>
        </div>
        <div class="col2">
          <div class="col-xs-6 col-sm-2 col1">
            <figure class="effect-bubba">
              <img class="img-responsive" src="{{ asset('img/Masaje/icon_2.png') }}" alt="" width="260" height="260"/>
              <figcaption>
                <h4>Traenos tu historial médicos</h4>
              </figcaption>     
            </figure>
          </div>
          <div class="col-xs-6 col-sm-2 col1">
            <figure class="effect-bubba">
              <img class="img-responsive" src="{{ asset('img/Masaje/icon_4.png') }}" alt="" width="260" height="260"/>
              <figcaption>
                <h4>Prueba nuestros productos</h4>  
              </figcaption>     
            </figure>
          </div>
          @if(Session::has('message'))
            <div class="col-md-8 col-md-offset-2">
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
                {{Session::get('message')}}
              </div>
            </div>
          @endif
          <div class="col-xs-6 col-xs-offset-3">
            <form role="form" method="post">
            @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Error!</strong> No se ha podido reservar.<br><br>
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
            </ul>
          </div>
          @endif
          <br>
          <br>
                    <label>Reservar</label>
                    <div class="form-group">
                        <div class='center input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control" name="fecha" />
                            <span class="indigo blanco center input-group-addon">
                                <span class="center glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-default" name="botonenviar">Reservar</button> 
            </form>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
</div>
<br>
<br>
@endsection