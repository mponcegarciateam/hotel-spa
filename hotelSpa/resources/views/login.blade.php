<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hotel Spa</title>

	<!-- CSS -->

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/hamburgers.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<!-- Scripts -->

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('/js/funciones.js') }}"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="collaps navbar-collaps" id="burger-div">
		<div class="burger-first-div">
			<a class="navbar-header"><img id="burger-logo" src="{{ asset('/img/logos/logofinal.png') }}"></a>
		</div>
		<div class="burger-second-div">
			<ul>
				@if (Auth::guest())
					<li><a href="#"><i class="fa fa-home" aria-hidden="true"></i>Bienvenido</a></li>
					<li><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a></li>
				@endif
			</ul>
		</div>
	</div>
	<nav id="navbar" class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header navbar-left">
				<button class="hamburger hamburger--elastic js-hamburguer navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse" id="hamburger-menu">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
			</div>
		</div>
	</nav>

	<div id="login">
		<video autoplay="autoplay" loop="loop" id="video" preload="auto"/>
	  		<source src="{{ asset('/videos/playa.mp4') }}" type="video/mp4" />
		</video/>
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-xs-offset-3 fondo-login">
					@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Error!</strong> Usuario o contraseña incorrectos.<br><br>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
					@endif

					<form class="form" role="form" method="POST" action="{{ url('/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div>
						    <label>DNI </label><br><input class="form-control" name="dni" type="text" required>
						</div>
						<div>
							<label>Nº Hab </label><br><input class="form-control" name="habit" type="text" required>
						</div>
						<button name="botonlogin" type="submit" class="btn">Login</button>
					</form>
				</div>
				@if(Session::has('message'))
			<br>
			<br>
            <div class="col-md-6 col-md-offset-3">
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
                {{Session::get('message')}}
              </div>
            </div>
          @endif
			</div>
			
		</div>
	</div>
	<script>hamburguesa();</script>
	<script>$(document).ready(function() {
        $('#hamburger-menu').click(function() {
        	$("#burger-div").delay(300).fadeIn();
        });
    });

    $(document).mouseup(function (e){
	    if (!$("#burger-div").is(e.target) && $("#burger-div").has(e.target).length === 0) {
	        $("#burger-div").hide(500);
	        $(".hamburger").removeClass("is-active");
	    }
	});</script>
</body>
</html>