@extends('app')

@section('content')
<div id="gimnasio">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    	<div class="carousel-inner" role="listbox">
        	<div class="item active">
        		<img src= "{{asset('/img/gimnasio/gimnasio.jpeg')}}" alt="pesas">
        		<div class="carousel-caption">
            		<h3>Zona de musculación</h3>
            	</div>
        	</div> 
	        <div class="item">
	        	<img src="{{asset('/img/gimnasio/pilates.jpg')}}" alt="yoga">
	        	<div class="carousel-caption">
	            	<h3>Zona de pilates</h3>
	        	</div> 
	        </div>
	    	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    		<span class="sr-only">Previous</span>
	    	</a>
	    	<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	        	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	        	<span class="sr-only">Next</span>
	    	</a>
	    </div>
    </div>
    <br>
    <br>
    <div class="container gimnasios">
    	<br>
    	<br>
		<div class="row">
			<div class="col-sm-4 center">
				<img src="{{asset('/img/gimnasio/cardiograma.png')}}">
				<h2>Cardio</h2>
			</div>
			<div class="col-sm-4 center">	
				<img src="{{asset('/img/gimnasio/medicine-ball.png')}}">
				<h2>Pilates</h2>
			</div>
			<div class="col-sm-4 center">
				<img src="{{asset('/img/gimnasio/peso-2.png')}}">
				<h2>Control de peso</h2>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-4 center">
				<img src="{{asset('/img/gimnasio/silbar.png')}}">
				<h2>Entrenadores</h2>
			</div>
			<div class="col-sm-4 center">
				<img src="{{asset('/img/gimnasio/ducha.png')}}">
				<h2>Duchas</h2>
			</div>
			<div class="col-sm-4 center">
				<img src="{{asset('/img/gimnasio/armario.png')}}">
				<h2>Taquillas</h2>
			</div>
		</div>
		<br>
		<div class="row center">
			@if(Session::has('message'))
	            <div class="col-md-8 col-md-offset-2">
	              <div class="alert alert-danger alert-dismissible" role="alert">
	                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
	                {{Session::get('message')}}
	              </div>
	            </div>
	        @endif
			<div class="col-sm-6 col-sm-offset-3">
				<br>
				<form role="form" method="post">
        		@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha podido reservar.<br><br>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
					@endif
                    <label>Reservar</label>
                    <div class="form-group">
                        <div class='center input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control" name="fecha" />
                            <span class="indigo blanco center input-group-addon">
                                <span class="center glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            		<button type="submit" class="btn btn-default" name="botonenviar">Reservar</button> 
        		</form>
        		<br>
        		<br>
	        </div>
	    </div>
	</div>
</div>

@endsection