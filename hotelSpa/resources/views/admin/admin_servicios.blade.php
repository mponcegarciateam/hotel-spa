@extends('layouts.admintempl')
@section('content')

@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
		{{Session::get('message')}}
	</div>
@endif

<div class="mother-grid-inner">
             <!--header start here-->
				<div class="header-main">
					<ol class="breadcrumb">
                		<li class="breadcrumb-item"><a href="{!!URL::to('/admin')!!}">Inicio </a> <i class="fa fa-angle-right"></i></li>
						<li class="breadcrumb-item"><a href="{!!URL::to('/admin/actividades')!!}">Actividades</a> <i class="fa fa-angle-right"></i></li>
            		</ol>
				</div>
</div>

<table class="table">
	<thead>
		<th>ID</th>
		<th>Servicio</th>
		<th>ID Cliente</th>
		<th>Observacion</th>
		<th>Operacion</th>
	</thead>	

	@foreach($m_servicios as $servicio)
	<tbody>
		<td>{{$servicio->id}}</td>
		<td>{{$servicio->serv}}</td>
		<td>{{$servicio->id_cliente}}</td>
		<td>{{$servicio->observacion}}</td>
		<td>{!!link_to_route('servicio.edit', $title = 'Modificar', $parameters = $servicio->id, $attributes= ['class'=>'btn btn-primary'])!!}</td>	
	</tbody>
	@endforeach

</table>

@stop