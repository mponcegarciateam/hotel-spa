@extends('layouts.admintempl')
@section('content')

<div class="mother-grid-inner">
	<div class="header-main">
		<ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin')!!}">Inicio</a> <i class="fa fa-angle-right"></i></li>
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin/servicios')!!}">Servicios</a> <i class="fa fa-angle-right"></i></li>
            <li class="breadcrumb-item"><a href="#">Modificar servicios</a> <i class="fa fa-angle-right"></i></li>
        </ol>
	</div>
</div>

<br>

{!!Form::model($serv, ['route' => ['servicio.update',$serv->id], 'method'=>'PUT'])!!}

	@include('admin.admin_forms.forms_servicios')
	<div class="formcustom">
	{!!Form::submit('Actualizar',['class'=>"btn btn-primary"])!!}

	{!!Form::close()!!}

	{!!Form::open(['route' => ['servicio.destroy',$serv->id], 'method'=>'DELETE'])!!} 

	{!!Form::submit('Eliminar',['class'=>"btn btn-danger"])!!} 
	</div>
	{!!Form::close()!!}
	<br>

@stop