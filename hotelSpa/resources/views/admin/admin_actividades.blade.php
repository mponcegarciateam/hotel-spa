@extends('layouts.admintempl')
@section('content')

@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
		{{Session::get('message')}}
	</div>
@endif

<div class="mother-grid-inner">
             <!--header start here-->
				<div class="header-main">
					<ol class="breadcrumb">
                		<li class="breadcrumb-item"><a href="{!!URL::to('/admin')!!}">Inicio </a> <i class="fa fa-angle-right"></i></li>
						<li class="breadcrumb-item"><a href="{!!URL::to('/admin/actividades')!!}">Actividades</a> <i class="fa fa-angle-right"></i></li>
            		</ol>
				</div>
</div>

<table class="table">
	<thead>
		<th>ID</th>
		<th>Actividad</th>
		<th>ID Cliente</th>
		<th>Fecha</th>
		<th>Operacion</th>
	</thead>	

	@foreach($m_actividades as $actividad)
	<tbody>
		<td>{{$actividad->id}}</td>
		<td>{{$actividad->actividad}}</td>
		<td>{{$actividad->id_cliente}}</td>
		<td>{{$actividad->fecha}}</td>
		<td>{!!link_to_route('actividad.edit', $title = 'Modificar', $parameters = $actividad->id, $attributes= ['class'=>'btn btn-primary'])!!}</td>	
	</tbody>
	@endforeach

</table>

@stop