<div class="form-group">
	{!!Form::label('Actividades:')!!}
	{!!Form::text('actividad',null,['class'=>"form-control", 'placeholder'=>"Escribe el servicio del usuario", 'maxlength'=>"50"])!!}
</div>

<div class="form-group">
    <div>
	{!!Form::label('ID Cliente:')!!}
    </div>
	<select name="id_cliente">
    @foreach($m_actividades as $id_cliente => $id_cli)
        @if ($modificar)
             @foreach($activi_sel as $id_cliente_mod => $id_cli_mod)
                @if ($id_cli_mod === $id_cli)
                    <option name="{{$id_cli_mod}}" selected>{{$id_cli_mod}}</option>
                @else
                    @if($id_cli !== 1)
                        <option name="{{$id_cli}}">{{$id_cli}}</option>
                    @endif
                @endif
             @endforeach
        @else
            @if($id_cli !== 1)
             <option name="{{$id_cli}}">{{$id_cli}}</option>
            @endif
        @endif
    @endforeach
	</select>

</div>
<div class="form-group">
	{!!Form::label('Fecha:')!!}
	{!!Form::text('fecha',null,['class'=>"form-control", 'placeholder'=>"DD/MM/YY", 'maxlength'=>"8"])!!}
</div>