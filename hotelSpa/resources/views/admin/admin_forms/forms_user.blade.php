<div class="form-group">
	{!!Form::label('DNI:')!!}
	{!!Form::text('dni',null,['class'=>"form-control", 'placeholder'=>"Escribe el DNI del usuario", 'maxlength'=>"9"])!!}
</div>

<div class="form-group">
	{!!Form::label('Habit:')!!}
	{!!Form::number('habit',null,['class'=>"form-control", 'placeholder'=>"Escribe la habitacion del usuario", 'min'=>"0", 'max'=>"9999"])!!}
</div>