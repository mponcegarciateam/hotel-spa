<div class="form-group">
	{!!Form::label('Servicio:')!!}
	{!!Form::text('serv',null,['class'=>"form-control", 'placeholder'=>"Escribe el servicio del usuario", 'maxlength'=>"50"])!!}
</div>

<div class="form-group">

	{!!Form::label('ID Cliente:')!!}
	<select name="id_cliente">
    @foreach($m_servicios as $id_cliente => $id_cli)
        @if ($modificar)
             @foreach($serv_sel as $id_cliente_mod => $id_cli_mod)
                @if ($id_cli_mod === $id_cli)
                    <option name="{{$id_cli_mod}}" selected>{{$id_cli_mod}}</option>
                @else
                    @if($id_cli !== 1)
                        <option name="{{$id_cli}}">{{$id_cli}}</option>
                    @endif
                @endif
             @endforeach
        @else
             @if($id_cli !== 1)
                <option name="{{$id_cli}}">{{$id_cli}}</option>
            @endif
        @endif
    @endforeach
	</select>

</div>
<div class="form-group">
	{!!Form::label('Observaciones:')!!}
	{!!Form::text('observacion',null,['class'=>"form-control", 'placeholder'=>"Escribe la observacion del usuario", 'maxlength'=>"100"])!!}
</div>