@extends('layouts.admintempl')
@section('content')

<div class="mother-grid-inner">
	<div class="header-main">
		<ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin')!!}">Inicio</a> <i class="fa fa-angle-right"></i></li>
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin/servicios')!!}">Servicios</a> <i class="fa fa-angle-right"></i></li>
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin/servicios/crear')!!}">Crear servicios</a> <i class="fa fa-angle-right"></i></li>
        </ol>
	</div>
</div>

<br>

{!!Form::open(['route'=>'servicio.store', 'method'=>'POST'])!!}

	@include('admin.admin_forms.forms_servicios')
	
	{!!Form::submit('Insertar',['class'=>"btn btn-primary"])!!}
{!!Form::close()!!}

<br>

@stop