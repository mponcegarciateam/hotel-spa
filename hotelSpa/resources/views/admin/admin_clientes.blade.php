@extends('layouts.admintempl')
@section('content')

@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
		{{Session::get('message')}}
	</div>
@endif

<div class="mother-grid-inner">
             <!--header start here-->
				<div class="header-main">
					<ol class="breadcrumb">
                		<li class="breadcrumb-item"><a href="{!!URL::to('/admin')!!}">Inicio </a> <i class="fa fa-angle-right"></i></li>
						<li class="breadcrumb-item"><a href="{!!URL::to('/admin/clientes')!!}">Clientes</a> <i class="fa fa-angle-right"></i></li>
            		</ol>
				</div>
</div>

<table class="table">
	<thead>
		<th>ID</th>
		<th>DNI</th>
		<th>Habit</th>
		<th>Operacion</th>
		<th>Ver PDF</th>
	</thead>	

	@foreach($users as $user)
	@if ($user->dni !== "12345678X")
	<tbody>
		<td>{{$user->id}}</td>
		<td>{{$user->dni}}</td>
		<td>{{$user->habit}}</td>
		<td>{!!link_to_route('usuario.edit', $title = 'Modificar', $parameters = $user->id, $attributes= ['class'=>'btn btn-primary'])!!}</td>
		<td>{!!link_to_route('pdf.edit', $title = 'Crear PDF', $parameters = $user->id, $attributes= ['class'=>'btn btn-info'])!!}</td>
	</tbody>
	@endif
	@endforeach

</table>

@stop