@extends('layouts.admintempl')
@section('content')

@if(Session::has('message'))
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
		{{Session::get('message')}}
	</div>
@endif

<br>

<div class="mother-grid-inner">
	<div class="header-main">
		<ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin')!!}">Inicio</a> <i class="fa fa-angle-right"></i></li>
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin/clientes')!!}">Clientes</a> <i class="fa fa-angle-right"></i></li>
            <li class="breadcrumb-item"><a href="{!!URL::to('/admin/clientes/crear')!!}">Crear clientes</a> <i class="fa fa-angle-right"></i></li>
        </ol>
	</div>
</div>

<br>

{!!Form::open(['route'=>'usuario.store', 'method'=>'POST'])!!}

	@include('admin.admin_forms.forms_user')
	
	{!!Form::submit('Insertar',['class'=>"btn btn-primary"])!!}
{!!Form::close()!!}

<br>

@stop