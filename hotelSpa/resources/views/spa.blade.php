@extends('app')

@section('content')
<div id="spa">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
    					<div class="item active">
      						<img src="{{ asset('img/spa/spa1.jpg') }}" alt="Spa1">
    					</div>
    					<div class="item">
    						<img src="{{ asset('img/spa/spa2.jpg') }}" alt="Spa2">
    					</div>
    					<div class="item">
    						<img src="{{ asset('img/spa/spa3.jpg') }}" alt="Spa3">
    					</div>
					</div>
					<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    					<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    					<span class="sr-only">Next</span>
					</a>
				</div>
				<br>
				<br>
			</div>
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 spa-columnas">
				<div class="col-sm-4 center">
					<img src="{{ asset('img/spa/jacuzzi.png') }}" alt="jacuzzi">
					<h2>Disfruta</h2>
					<p>Contamos con jacuzzis, piscinas, baños turcos...Todo lo necesario para que te des un homenaje a lo grande. Pruébalo!</p>
				</div>
				<div class="col-sm-4 center">
					<img src="{{ asset('img/spa/bathrobe.png') }}" alt="bathrobe">
					<h2>Relájate</h2>
					<p>Te mereces un rato de descanso, para ello nuestro spa dispone de todo lo necesario para que tu estancia sea agradable. Simplemente ven y olvídate de todo</p>
				</div>
				<div class="col-sm-4 center">
					<img src="{{ asset('img/spa/woman.png') }}" alt="woman">
					<h2>Reserva</h2>
					<p>Para disfrutar de nuestro spa deberás realizar una reserva. Abajo encontrarás un calendario para facilitarte las cosas. Reserva y a disfrutar!</p>
				</div>
			</div>
		</div>
    	<div class="row center">
    	@if(Session::has('message'))
            <div class="col-md-8 col-md-offset-2">
              <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
                {{Session::get('message')}}
              </div>
            </div>
          @endif
        	<div class="col-xs-6 col-xs-offset-3">
        	<br>  
        		<form role="form" method="post">
        		@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha podido reservar.<br><br>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
					@endif
                    <label>Reservar</label>
                    <div class="form-group">
                        <div class='center input-group date' id='datetimepicker2'>
                            <input type='text' class="form-control" name="fecha" />
                            <span class="indigo blanco center input-group-addon">
                                <span class="center glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            		<button type="submit" class="btn btn-default" name="botonenviar">Reservar</button> 
        		</form>
                <br>
                <br>
        	</div>
    	</div>
    </div>
		<br>
		<br>
</div>
@endsection