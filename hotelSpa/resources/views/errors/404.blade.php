<html>
	<head>
	<meta charset="utf-8">
	<title> 404 Not Found </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="notfound.css">
    <style>
    	#notfound {
			margin-top: 5%;
			text-align: center;
		}
		h1{
    		color: #555;
		}
    </style>
	</head>
	<body>
		<div id="notfound">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<img src="{{ asset('img/logos/palmera1.png') }}">
						<h1>404, not found</h1>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>