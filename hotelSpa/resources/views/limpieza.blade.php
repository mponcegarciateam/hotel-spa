@extends('app')

@section('content')

<div id="limpieza">
<table width="100%" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" background="{{ asset('img/limpieza/header.jpg') }}" style="background-size:cover; background-position:center;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
					<tr>
						<td style="background-color:rgba(21, 21, 21, 0.62);" data-bgcolor="Header">
							<table id="texttable" align="center">
								<!-- Headline -->
								<tr>
									<td class="title">
										<singleline label="headline">SERVICIO DE LIMPIEZA  <br> HOTEL SPA</singleline>
									</td>
								</tr>
								<!-- end Headline -->
								<tr>
									<td height="10"></td>
								</tr>
								<br>
								<br>
								<!-- end sub headline -->
								<tr>
									<td height="20"></td>
								</tr>  
								<tr>
									<td height="20"></td>
								</tr>
								<tr>
									<td height="20"></td>
								</tr>
								<br>
								<br>
								<br>
								<br>
							</table>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
	<br>
	<div class="container">
	<br>
	<div class="col-sm-8 col-sm-offset-2">
	  <div class="row center">
	    <div class="col-sm-6 col-limpieza">
	    	<img src="{{ asset('img/limpieza/icon_1.png') }}" width="100" alt="Image">
	    	<h2>Maquinaria de calidad</h2>
	    	<p>En hotelSpa disponemos de una maquinaria sencilla pero implacable y rápida.</p>
	    </div>
	    <div class="col-sm-6 col-limpieza">
	    	<img src="{{ asset('img/limpieza/icon_2.png') }}" width="100" alt="Image">
	    	<h2>Personal implacable</h2>
	    	<p>El personal de limpieza está totalmente capacitado para dejar tu habitación totalmente reluciente en poco tiempo.</p>
	    </div>
	  </div>
	  <br>
	  <br>
	  @if(Session::has('message'))
				<div class="col-md-8 col-md-offset-2">
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
						{{Session::get('message')}}
					</div>
				</div>
			@endif
	  <div class="row">
		<form class="col-xs-8 col-xs-offset-2 center" method="post">
			@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha podido guardar.<br><br>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
					@endif
				<div class="row center">
					<p>Selecciona una hora:</p>
				  	<div class="input-group clockpicker col-sm-4 col-sm-offset-4">
					  	<input type="text" class="form-control bordeBoton centrado" readonly="" style="background-color: white" value="08:00" id="hora">
							<span class="input-group-addon indigo blanco bordeBoton">
							    <span class="glyphicon glyphicon-time"></span>
							</span>
					</div>
				
					
				</div>
				<br>
				<div class="row center">
					<div class="form-group">
					  <label for="comment">Observaciones:</label>
					  <textarea name="observacion" class="form-control" rows="5" id="observacion">Quiero que limpien mi habitación a las 08:00. </textarea>
					</div>
				</div>
				<br>
				<div class="row center">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button name="botonenviar" type="submit" class="btn btn-default">Guardar</button>
				</div>
				<br>
			</form>
		</div>
	</div>
	<script>reloj2();</script>
	</div>
	<br>
	<br>
</div>

@endsection