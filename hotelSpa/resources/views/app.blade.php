<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hotel Spa</title>

	<!-- CSS -->

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/hamburgers.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/bootstrap-datapicker.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/bootstrap-clockpicker.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Scripts -->

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('/js/funciones.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-clockpicker.js') }}"></script>

<!-- scrip calendario jaime -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
</head>
<body>
	<div class="collaps navbar-collaps" id="burger-div">
		<div class="burger-first-div">
			<a class="navbar-header"><img id="burger-logo" src="{{ asset('/img/logos/logofinal.png') }}"></a>
		</div>
		<div class="burger-second-div">
			<ul>
				@if (Auth::guest())
					<li><a href="#"><i class="fa fa-home" aria-hidden="true"></i>Welcome/a></li>
					<li><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a></li>
				@else
					<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>{{ Auth::user()->dni }}</a></li>
					<li><a href="{{ url('/home') }}"><i class="fa fa-home" aria-hidden="true"></i>Inicio</a></li>
					<li><a href="{{ url('/servicios/comida') }}"><i class="fa fa-apple" aria-hidden="true"></i>Comida</a></li>
					<li><a href="{{ url('/servicios/despertador') }}"><i class="fa fa-clock-o" aria-hidden="true"></i>Despertador</a></li>
					<li><a href="{{ url('/servicios/limpieza') }}"><i class="fa fa-bed" aria-hidden="true"></i>Limpieza</a></li>
					<li><a href="{{ url('/actividades/gimnasio') }}"><i class="fa fa-heart" aria-hidden="true"></i>Gimnasio</a></li>
					<li><a href="{{ url('/actividades/conferencias') }}"><i class="fa fa-users" aria-hidden="true"></i>Conferencias</a></li>
					<li><a href="{{ url('/actividades/spa') }}"><i class="fa fa-shower" aria-hidden="true"></i>Spa</a></li>
					<li><a href="{{ url('/actividades/masajista') }}"><i class="fa fa-sign-language" aria-hidden="true"></i>Masajista</a></li>
					<li><a href="{{ url('/logout') }}"><i class="fa fa-times" aria-hidden="true"></i>Logout</a></li>
				@endif
			</ul>
		</div>
	</div>
	<nav id="navbar" class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header navbar-left">
				<button class="hamburger hamburger--elastic js-hamburguer navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse" id="hamburger-menu">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
			</div>
		</div>
	</nav>

	@yield('content')
	
	<footer>
		<div id="prefooter">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-3">
						<h2>Servicios</h2>
						<ul>
							<li><a href="{{ url('/servicios/comida') }}">Servicio de comida y bebida</a></li>
							<li><a href="{{ url('/servicios/limpieza') }}">Servicio de limpieza</a></li>
							<li><a href="{{ url('/servicios/despertador') }}">Servicio de despertador</a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-3">
						<h2>Actividades</h2>
						<ul>
							<li><a href="{{ url('/actividades/gimnasio') }}">Gimnasio</a></li>
							<li><a href="{{ url('/actividades/masajista') }}">Masajista</a></li>
							<li><a href="{{ url('/actividades/conferencias') }}">Conferencias</a></li>
							<li><a href="{{ url('/actividades/spa') }}">SPA</a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-4 col-sm-offset-2">
						<h2>Redes sociales</h2>
						<ul class="social">
	                        <li><a href="https://www.facebook.com/"><i class=" fa fa-facebook"></i></a></li>
	                        <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
	                        <li><a href="https://plus.google.com/?hl=es"><i class="fa fa-google-plus"></i></a></li>
	                        <li><a href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
	                        <li><a href="https://www.instagram.com/?hl=es"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p>Copyright 2017 © Grupo Black Ops<span><a href="#">Política de cookies</a></span><span><a href="#">Política de privacidad</a></span></p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- Scripts -->
	<script>hamburguesa();</script>
	<script>
		$(document).ready(function() {
        $('#hamburger-menu').click(function() {
        	$("#burger-div").delay(300).fadeIn();
        });
    });

    $(document).mouseup(function (e){
	    if (!$("#burger-div").is(e.target) && $("#burger-div").has(e.target).length === 0) {
	        $("#burger-div").hide(500);
	        $(".hamburger").removeClass("is-active");
	    }
	});
	</script>
	<script>
        $(function () {
         var date = new Date();
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YY',
                defaultDate: date,
                minDate: date
            });
        });
    </script>
</body>
</html>
