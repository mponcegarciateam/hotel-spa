@extends('app')

@section('content')
<div id="comida">
	<table width="100%" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center" background="{{ asset('img/comida/header.jpg') }}" style="background-size:cover; background-position:center;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
					<tr>
						<td style="background-color:rgba(21, 21, 21, 0.62);" data-bgcolor="Header">
							<table align="center">
								<tr>
									<td height="50" class="td-low"></td>
								</tr>
								<!-- Headline -->
								<tr>
									<td class="title">
										<singleline label="headline">SERVICIO DE COMIDA  <br> HOTEL SPA</singleline>
									</td>
								</tr>
								<!-- end Headline -->
								<tr>
									<td height="10"></td>
								</tr> 
								<!-- sub headline -->
								<tr>
									<td class="eslogan">
										<singleline label="sub-headline">Rapidez y calidad</singleline>
									</td>
								</tr>
								<!-- end sub headline -->
								<tr>
									<td height="20"></td>
								</tr>  
								<tr>
									<td height="20"></td>
								</tr>
								<tr>
									<td align="center">
										<!-- button --> 
										<table class="textbutton" align="center" border="0" cellpadding="0" cellspacing="0" >
											<tr>
												<td align="center" style="">
													<button id="buttonmodal" type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">Ver carta</button>
													<br>
													<br>

													<!-- Modal -->
													<div id="myModal" class="modal fade" role="dialog">
													  <div class="modal-dialog">

													    <!-- Modal content-->
													    <div class="modal-content">
													      <div class="modal-header">
													        <button type="button" class="close" data-dismiss="modal">&times;</button>
													        <h1 class="modal-title">Carta Hotel Spa</h1>
													      </div>
													      <div class="modal-body">
													        <h3>Primer plato.</h3>
													        <p>-Sopa de tomate</p>
													        <p>-Fabada asturiana</p>
													        <p>-Ensalada de pasta</p>
													        <p>-Cóctel de gambas</p>
													        <h3>Segundo plato.</h3>
													        <p>-Huevos a la flamenca</p>
													        <p>-Paletilla de cabrito al horno con ensalada</p>
													        <p>-Salmón al horno con patatas</p>
													        <h3>Postre.</h3>
													        <p>-Flan de Calabaza y Caramelo</p>
													        <p>-Pastel Tres Leches</p>
													        <p>-Galletas de Chocolate de Leche y Avena</p>
													        <h3>Bebidas</h3>
													        <p>-Refrescos</p>
													        <p>-Cerveza</p>
													        <p>-Zumos</p>
													      </div>
													      <div class="modal-footer">
													        <button id="buttonmodalcerrar" type="button" class="btn btn-default bordeBoton" data-dismiss="modal">Cerrar</button>
													      </div>
													    </div>

													  </div>
													</div>
												</td>
											</tr>
										</table> 
										<!-- end button -->
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
	<br>
	<br>
	<div class="container">
			<div class="row">
			    <div class="col-sm-4 center">
			    	<img src="{{ asset('img/comida/icon_1.png') }}" width="100" alt="Image">
			    	<h2>Primer plato</h2>
			    	<p>Sirvete de nuestros platos entrantes que harán que tu apetito entre en calma.</p>
			    </div>
			    <div class="col-sm-4 center">
			    	<img src="{{ asset('img/comida/icon_2.png') }}" width="100" alt="Image">
			    	<h2>Segundo plato</h2>
			    	<p>Preparate para disfrutar con nuestra gama de platos principales.</p>
			    </div>
			    <div class="col-sm-4 center">
			    	<img src="{{ asset('img/comida/icon_3.png') }}" width="100" alt="Image">
			    	<h2>Postre</h2>
			    	<p>Deliciosos aperitivos con elaborados con todo el cariño por el personal para que quedes totalmente satisfecho.</p>
			    </div>
		</div>
	  	<div class="row center">
	  		@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha podido reservar.<br><br>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
					@endif
			<form role="form" id="" class="col-sm-8 col-sm-offset-2" method="post">
				<div class="form-group">
					<h2>Observaciones:</h3>
					@if(Session::has('message'))
						<div class="col-md-8 col-md-offset-2">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
								{{Session::get('message')}}
							</div>
						</div>
						@endif
					<br>
					<textarea rows="5" name="observacion" class="form-control" required></textarea>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button id="botoncomida" name="botonenviar" type="submit" class="btn btn-default">Reservar</button>
			</form>
		</div>
		<br>
		<br>
	</div>
</div>
@endsection