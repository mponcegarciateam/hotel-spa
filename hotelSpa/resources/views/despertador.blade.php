@extends('app')

@section('content')
	<div id="despertador">
		<br>
		<br>
		<div class="container" id="contenedor">
			<div class="row">
				<h2 class="center">Servicio de Despertador</h2><br>
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2 tarjeta">
						<img class="col-xs-2" src="{{ asset('/img/despertador/despertador.png') }}">
						<p class="col-xs-10" style="padding: 5px">HotelSpa dispone de sistemas automatizados y protocolos para que usted pueda despertarse cuando nos indique. A la hora señalada, y de forma totalmente automática, el sistema llamará al número de su habitación. Cuando descuelgue el auricular, la centralita responderá con un mensaje de buenos días y la hora.</p>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2 tarjeta">
						<p class="col-xs-10" style="padding: 5px">Nuestro personal de recepción siempre está atento a tus necesidades. Si nadie responde a la llamada, el personal de recepción acudirá personalmente a la habitación para conocer el motivo de la incidencia y notificarle la hora que es.</p>
						<img class="col-xs-2" src="{{ asset('/img/despertador/recepcionista.png') }}">
					</div>
				</div>
				<br><br>
			</div>

			@if(Session::has('message'))
				<div class="col-md-6 col-md-offset-3">
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span ariahidden="true">&times;</span></button>
						{{Session::get('message')}}
					</div>
				</div>
			@endif
			
			<form class="col-xs-8 col-xs-offset-2 center" method="post">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha podido guardar.<br><br>
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
					@endif
				<div class="row">
					<p class="centrado">Selecciona una hora:</p>
				  	<div class="input-group clockpicker col-sm-4 col-sm-offset-4">
					  	<input type="text" class="form-control center" readonly="" style="background-color: white" value="08:00" id="hora">
							<span class="input-group-addon indigo blanco">
							    <span class="glyphicon glyphicon-time"></span>
							</span>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="form-group">
					  <label for="comment">Observaciones:</label>
					  <textarea name="observacion" class="form-control" rows="5" id="observacion">Quiero que me despierten a las 08:00. </textarea>
					</div>
				</div>
				<br>
				<div class="row">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button name="botonenviar" type="submit" class="btn btn-default">Guardar</button>
				</div>
				<br>
			</form>
		</div>
		<script>reloj();</script>
	</div>
	<br><br>
@endsection
