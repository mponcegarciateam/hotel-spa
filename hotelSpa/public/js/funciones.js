function hamburguesa() {
	var $hamburger = $(".hamburger");
	$hamburger.on("click", function(e) {
    	$hamburger.toggleClass("is-active");
  	});
}

function aparecer() {
	$(document).ready(function() {
        $('#hamburger-menu').click(function() {
        	$("#burger-div").delay(300).fadeIn();
        });
    });

    $(document).mouseup(function (e){
	    if (!$("#burger-div").is(e.target) && $("#burger-div").has(e.target).length === 0) {
	        $("#burger-div").hide();
	    }
	});
}

function reloj() {
	$('.clockpicker').clockpicker({
		placement: 'bottom',
		align: 'left',
		default: '08:00',
		autoclose: true,
		afterDone: function() {
			hora = document.getElementById("hora").value;
            document.getElementById("observacion").value = "Quiero que me despierten a las " + hora + ". ";
        }
	});
}

function reloj2() {
	$('.clockpicker').clockpicker({
		placement: 'bottom',
		align: 'left',
		default: '08:00',
		autoclose: true,
		afterDone: function() {
			hora = document.getElementById("hora").value;
            document.getElementById("observacion").value = "Quiero que me limpien mi habitación a las " + hora + ". ";
        }
	});
}

function redireccionar() {
    setTimeout("location.href='/home'", 4000);
}