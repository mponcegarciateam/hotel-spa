<?php namespace hotelSpa\Http\Controllers;

use hotelSpa\Http\Requests;
use hotelSpa\Http\Controllers\Controller;

use Session;
use Redirect;
use Auth;

use Illuminate\Http\Request;

class ActividadController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function admin_actividades()
	{
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				$m_actividades = \hotelSpa\actividades::All();
				return view('admin.admin_actividades',compact('m_actividades'));
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				$m_actividades = \hotelSpa\User::orderBy('id')->lists('id');
				$activi_sel = False;
				$activi = False;
				$modificar = False;
				return view('admin.admin_crear_actividades',compact('m_actividades'), ['activi'=>$activi, 'activi_sel'=>$activi_sel, 'modificar'=>$modificar]);
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$fecha = $request['fecha'];

		$dia = intval(substr($fecha, 0, 2));
		$mes = intval(substr($fecha, 3, 2));
		$anno = intval("20".substr($fecha, 6, 2)); 

		if (checkdate($mes, $dia, $anno)){
			\hotelSpa\actividades::create([
			'actividad' => $request['actividad'],
			'id_cliente' => $request['id_cliente'],
			'fecha' => $request['fecha'],
			]);

			Session::flash('message','Actividad creada correctamente');
			return Redirect::to('/admin/actividades');
		}

		Session::flash('message','Formato incorrecto');
		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$m_actividades = \hotelSpa\User::orderBy('id')->lists('id');
		$activi_sel = \hotelSpa\actividades::where('id', '=', $id)->lists('id_cliente');
		$activi = \hotelSpa\actividades::find($id);
		$modificar = True;
		return view('admin.admin_modificar_actividades', ['activi'=>$activi, 'm_actividades'=>$m_actividades, 'activi_sel'=>$activi_sel, 'modificar'=>$modificar]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$m_actividades = \hotelSpa\actividades::find($id);
		$fecha = $request['fecha'];

		$dia = intval(substr($fecha, 0, 2));
		$mes = intval(substr($fecha, 3, 2));
		$anno = intval("20".substr($fecha, 6, 2)); 

		if (checkdate($mes, $dia, $anno)){
			$m_actividades->fill($request->all());
			$m_actividades->save();

			Session::flash('message','Actividad editado correctamente');
			return Redirect::to('/admin/actividades');
		}

		Session::flash('message','Formato incorrecto');
		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\hotelSpa\actividades::destroy($id);
		Session::flash('message','Actividad eliminado correctamente');
		return Redirect::to('/admin/actividades');
	}

}