<?php namespace hotelSpa\Http\Controllers;
use hotelSpa\servicios;
use hotel\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Auth;

class LimpiezaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Limpieza Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		/*$this->middleware('guest');*/
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check() == false) {
			return Redirect::to('login');
		}
		return view('limpieza');
	}

	public function create()
	{
		if (isset($_POST['botonenviar'])){
            $id = Auth::user()->id;
			$registro=servicios::where('serv', '=', 'Limpieza')->where('id_cliente', '=', $id)->first();
			if ($registro){
				Session::flash('message','No se ha podido reservar el servicio de limpieza');
				return Redirect::back();
			}
			else{
				$servicio= new servicios;
	            $servicio -> serv = 'Limpieza';
	            $servicio -> id_cliente = $id;
	            $servicio -> observacion = Input:: get('observacion');
	            $servicio -> save();
	            //Session::flash('message','Servicio de limpieza reservado correctamente');
	            return view('redirect');
			}
        }
	}

}
