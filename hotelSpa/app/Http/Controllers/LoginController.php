<?php namespace hotelSpa\Http\Controllers;

use hotelSpa\User;
use hotel\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use Auth;
use Redirect;
use Session;

class LoginController extends Controller {

/*
|--------------------------------------------------------------------------
| Login Controller
|--------------------------------------------------------------------------
|
| This controller renders your application's "dashboard" for users that
| are authenticated. Of course, you are free to change or remove the
| controller as you wish. It is just here to get your app started!
|
*/

/**
* Create a new controller instance.
*
* @return void
*/
/**
* Show the application dashboard to the user.
*
* @return Response
*/
  public function index()
  {
    return view('login');
  }

  public function logout()
  {
    Auth::logout();
    return Redirect::to('login');
  }

  public function login()
  {
    if (isset($_POST['botonlogin'])){
      $dni = Input:: get('dni');
      $habit = Input:: get('habit');
      $user = User::where('dni', '=', $dni)->where('habit', '=', $habit)->first();
      if ($user) {
        Auth::login($user);
        return Redirect::to('home');
      }
      Session::flash('message','Error al iniciar sesión');
      return Redirect::to('login');
    }
  }
}
