<?php namespace hotelSpa\Http\Controllers;

use hotelSpa\Http\Requests;
use hotelSpa\Http\Controllers\Controller;

use Session;
use Redirect;
use Auth;

use Illuminate\Http\Request;

class UsuarioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function admin_clientes()
	{	
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				$users = \hotelSpa\User::All();
				return view('admin.admin_clientes',compact('users'));
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				return view('admin.admin_crear_clientes');
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$dni = $request['dni'];
		$letra = substr($dni, -1);
		$numeros = substr($dni, 0, -1);
		if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
			$valido = True;
		}else{
			$valido = False;
		}

		if ($valido){
			\hotelSpa\User::create([
				'dni' => $request['dni'],
				'habit' => $request['habit'],
			]);
			Session::flash('message','Usuario creado correctamente');
			return Redirect::to('/admin/clientes');
		}

		Session::flash('message','Formato de DNI incorrecto');
		return Redirect::back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = \hotelSpa\User::find($id);
		return view('admin.admin_modificar_clientes', ['user'=>$user]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$user = \hotelSpa\User::find($id);

		$dni = $request['dni'];
		$letra = substr($dni, -1);
		$numeros = substr($dni, 0, -1);
		if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
			$valido = True;
		}else{
			$valido = False;
		}

		if ($valido){
			$user = \hotelSpa\User::find($id);
			$user->fill($request->all());
			$user->save();

			Session::flash('message','Usuario creado correctamente');
			return Redirect::to('/admin/clientes');
		}

		Session::flash('message','Formato de DNI incorrecto');
		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\hotelSpa\User::destroy($id);
		Session::flash('message','Usuario eliminado correctamente');
		return Redirect::to('/admin/clientes');
	}

}