<?php namespace hotelSpa\Http\Controllers;

use hotelSpa\Http\Requests;
use hotelSpa\Http\Controllers\Controller;

use Session;
use Redirect;
use Auth;

use Illuminate\Http\Request;

class ServicioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function admin_servicios()
	{
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				$m_servicios = \hotelSpa\servicios::All();
				return view('admin.admin_servicios',compact('m_servicios'));
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				$m_servicios = \hotelSpa\User::orderBy('id')->lists('id');
				$serv_sel = False;
				$serv = False;
				$modificar = False;
		return view('admin.admin_crear_servicios',compact('m_servicios'), ['serv'=>$serv, 'serv_sel'=>$serv_sel, 'modificar'=>$modificar]);
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		\hotelSpa\servicios::create([
			'serv' => $request['serv'],
			'id_cliente' => $request['id_cliente'],
			'observacion' => $request['observacion'],
		]);
		//Session::flash('message','Servicio creado correctamente');
		return Redirect::to('/admin/servicios');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$m_servicios = \hotelSpa\User::orderBy('id')->lists('id');
		$serv_sel = \hotelSpa\servicios::where('id', '=', $id)->lists('id_cliente');
		$serv = \hotelSpa\servicios::find($id);
		$modificar = True;
		return view('admin.admin_modificar_servicios', ['serv'=>$serv, 'm_servicios'=>$m_servicios, 'serv_sel'=>$serv_sel, 'modificar'=>$modificar]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$m_servicios = \hotelSpa\servicios::find($id);
		$m_servicios->fill($request->all());
		$m_servicios->save();

		Session::flash('message','Servicio editado correctamente');
		return Redirect::to('/admin/servicios');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		\hotelSpa\servicios::destroy($id);
		Session::flash('message','Servicio eliminado correctamente');
		return Redirect::to('/admin/servicios');
	}

}
