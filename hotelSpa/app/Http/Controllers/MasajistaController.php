<?php namespace hotelSpa\Http\Controllers;
use hotelSpa\actividades;
use hotel\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use Session;
use Redirect;
use Auth;

class MasajistaController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Masajista Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		/*$this->middleware('guest');*/
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check() == false) {
			return Redirect::to('login');
		}
		return view('masajista');
	}
	public function create()
	{
		if (isset($_POST['botonenviar'])){
            $id = Auth::user()->id;
			$registro=actividades::where('actividad', '=', 'Masajista')->where('id_cliente', '=', $id)->first();
			if ($registro){
				Session::flash('message','No se ha podido reservar la actividad de masaje');
				return Redirect::back();
			}
			else{
				$actividad= new actividades;
	            $actividad -> actividad = 'Masajista';
	            $actividad -> id_cliente = $id;
	            $actividad -> fecha = Input:: get('fecha');
	            $actividad -> save();
	            //Session::flash('message','Actividad de masaje reservado correctamente');
	            return view('redirect');
			}
        }
	}
}
