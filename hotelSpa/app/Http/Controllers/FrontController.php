<?php namespace hotelSpa\Http\Controllers;

use hotelSpa\Http\Requests;
use hotelSpa\Http\Controllers\Controller;

use Auth;
use Redirect;

use Illuminate\Http\Request;

class FrontController extends Controller {

	public function admin(){
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				return view('admin.index');
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	public function admin_clientes(){
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				return view('admin.admin_clientes');
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	public function admin_crear_clientes(){
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				return view('admin.admin_crear_clientes');
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	public function admin_modificar_clientes(){
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				return view('admin.admin_modificar_clientes');
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}

	public function admin_eliminar_clientes(){
		if(Auth::check()){
			if(Auth::User()->dni=='12345678X'){
				return view('admin.admin_eliminar_clientes');
			}
			else{
				return Redirect::to('home');
			}
		}
		else {
			return Redirect::to('login');
		}
	}


}
