<?php namespace hotelSpa\Http\Controllers;

use Illuminate\Http\Request;

use hotelSpa\Http\Requests;
use hotelSpa\Http\Controllers\Controller;


class PdfController extends Controller
{

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario_pdf = \hotelSpa\User::where('id', '=', $id)->first();
        $servicio_pdf = \hotelSpa\servicios::where('id_cliente', '=', $id)->orderBy('id')->get();
        $actividad_pdf = \hotelSpa\actividades::where('id_cliente', '=', $id)->orderBy('fecha')->get();

        $view =  \View::make('pdf.informe_usuario', compact('usuario_pdf', 'servicio_pdf', 'actividad_pdf'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        return $pdf->stream('informe');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
