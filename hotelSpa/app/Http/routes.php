<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('login','LoginController@index');
Route::post('login','LoginController@login');
Route::get('logout','LoginController@logout');

//Route::get('redirect','RedirectController@index');

/* Actividades */

Route::get('actividades/masajista', 'MasajistaController@index');
Route::post('actividades/masajista', 'MasajistaController@create');

Route::get('actividades/conferencias','ConferenciasController@index');
Route::post('actividades/conferencias','ConferenciasController@create');

Route::get('actividades/spa','SpaController@index');
Route::post('actividades/spa','SpaController@create');

Route::get('actividades/gimnasio','GimnasioController@index');
Route::post('actividades/gimnasio','GimnasioController@create');

/* Servicios */

Route::get('servicios/comida','ComidaController@index');
Route::post('servicios/comida','ComidaController@create');

Route::get('servicios/despertador','DespertadorController@index');
Route::post('servicios/despertador','DespertadorController@create');

Route::get('servicios/limpieza','LimpiezaController@index');
Route::post('servicios/limpieza','LimpiezaController@create');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/* Get del Panel de Administracion */

Route::get('admin/clientes','UsuarioController@admin_clientes');

Route::get('admin/clientes/crear','UsuarioController@create');

Route::get('admin/actividades','ActividadController@admin_actividades');

Route::get('admin/actividades/crear','ActividadController@create');

Route::get('admin/servicios','ServicioController@admin_servicios');

Route::get('admin/servicios/crear','ServicioController@create');

/* Fin del Panel de Administracion */

/*-- Recursos --*/

Route::get('admin','FrontController@admin');

Route::resource('usuario','UsuarioController');

Route::resource('actividad','ActividadController');

Route::resource('servicio','ServicioController');

/*-- Fin de Recursos --*/

/*-- PDF --*/

Route::resource('pdf','PdfController');

/*-- PDF --*/