<?php namespace hotelSpa;

use Illuminate\Database\Eloquent\Model;

class servicios extends Model {

	protected $table = "servicios";


	protected $fillable = ['serv','id_cliente', 'observacion'];

}
