<?php namespace hotelSpa;

use Illuminate\Database\Eloquent\Model;

class actividades extends Model {

	protected $table = "actividades";

	protected $fillable = ['actividad','id_cliente', 'fecha'];
}
